/*
 * ds3231.h
 *
 *  Created on: Jan 30, 2019
 *      Author: Francis
 */

#ifndef DS3231_H_
#define DS3231_H_

#include <inttypes.h>
#include "stm32f3xx_hal.h"

typedef enum {
	AM = 0,
	PM = 1
} DS3231_AM_PM;

typedef enum {
	HOURS24 = 0,
	HOURS12 = 1
} DS3231_HourFormat;

typedef struct {
	uint8_t hours;
	uint8_t minutes;
	uint8_t seconds;
	DS3231_AM_PM am_pm;
	DS3231_HourFormat fmt;
} DS3231_Time;

typedef enum {
	SUNDAY = 1,
	MONDAY = 2,
	TUESDAY = 3,
	WEDNESDAY = 4,
	THURSDAY = 5,
	FRIDAY = 6,
	SATURDAY = 7
} DS3231_DayOfWeek;

typedef struct {
	uint8_t dom;
	DS3231_DayOfWeek dow;
	uint8_t month;
	uint8_t year;
	uint8_t century;
} DS3231_Date;

typedef union {
	uint8_t reg;
	struct {
		uint8_t a1f : 1;
		uint8_t a2f : 1;
		uint8_t en32 : 1;
		uint8_t reserved : 3;
		uint8_t osf : 1;
	};
} DS3231_Status;

void ds3231_get_time(I2C_HandleTypeDef *handle, DS3231_Time *time);
void ds3231_set_time(I2C_HandleTypeDef *handle, DS3231_Time *time);

void ds3231_get_date(I2C_HandleTypeDef *handle, DS3231_Date *date);
void ds3231_set_date(I2C_HandleTypeDef *handle, DS3231_Date *date);

void ds3231_get_temp(I2C_HandleTypeDef *handle, float *temp);

#endif /* DS3231_H_ */
