/*
 * courier_font.h
 *
 *  Created on: Jan 27, 2019
 *      Author: Francis
 */

#ifndef COURIER_FONT_H_
#define COURIER_FONT_H_

#include <inttypes.h>

typedef struct {
	uint8_t width;
	uint8_t height;
	uint16_t offset;
} FONT_CHAR_INFO;

typedef struct {
	uint8_t height;
	char start;
	char end;
	FONT_CHAR_INFO *descriptors;
	uint8_t *bitmaps;
} FONT_INFO;

/* Font data for Courier New 8pt */
extern const uint8_t courierNew_8ptBitmaps[];
extern const FONT_INFO courierNew_8ptFontInfo;
extern const FONT_CHAR_INFO courierNew_8ptDescriptors[];

#endif /* COURIER_FONT_H_ */
