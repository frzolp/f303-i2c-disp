/*
 * data_utils.h
 *
 *  Created on: Jan 30, 2019
 *      Author: Francis
 */

#ifndef DATA_UTILS_H_
#define DATA_UTILS_H_

#include <inttypes.h>
#include "stm32f3xx_hal.h"

void send_data(I2C_HandleTypeDef *handle, uint16_t address, uint8_t *data, uint16_t len);
void get_data(I2C_HandleTypeDef *handle, uint16_t address, uint8_t *data, uint16_t len);

#endif /* DATA_UTILS_H_ */
