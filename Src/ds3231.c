/*
 * ds3231.c
 *
 *  Created on: Jan 30, 2019
 *      Author: Francis
 */

#include "ds3231.h"
#include "data_utils.h"
#include <stdio.h>

#define DS3231_ADDR		(uint16_t)(0x68 << 1)

// RTC registers
#define DS3231_REG_SECONDS	(uint8_t)0x00
#define DS3231_REG_MINUTES	(uint8_t)0x01
#define DS3231_REG_HOURS	(uint8_t)0x02
#define DS3231_REG_DAY		(uint8_t)0x03
#define DS3231_REG_DATE		(uint8_t)0x04
#define DS3231_REG_MONTH	(uint8_t)0x05
#define DS3231_REG_YEAR		(uint8_t)0x06

// Alarm 1 registers
#define DS3231_REG_A1SECS	(uint8_t)0x07
#define DS3231_REG_A1MINS	(uint8_t)0x08
#define DS3231_REG_A1HOURS	(uint8_t)0x09
#define DS3231_REG_A1DAY	(uint8_t)0x0A

// Alarm 2 registers
#define DS3231_REG_A2MINS	(uint8_t)0x0B
#define DS3231_REG_A2HOURS	(uint8_t)0x0C
#define DS3231_REG_A2DAY	(uint8_t)0x0D

// Control/status registers
#define DS3231_REG_CONTROL	(uint8_t)0x0E
#define DS3231_REG_CTRLSTAT	(uint8_t)0x0F
#define DS3231_REG_AGING	(uint8_t)0x10
#define DS3231_REG_TEMPMSB	(uint8_t)0x11
#define DS3231_REG_TEMPLSB	(uint8_t)0x12

extern void _Error_Handler(char *name, const char *func, int line);

void ds3231_get_time(I2C_HandleTypeDef *handle, DS3231_Time *time) {
	uint8_t buf[2] = { 0 };
	buf[0] = DS3231_REG_SECONDS;
	// Request the contents of the seconds register (0x0)
	get_data(handle, DS3231_ADDR, buf, 1);

	// Convert BCD into int: multiply upper nibble by 10, add to lower nibble
	time->seconds = (buf[0] & 0x0F) + (((buf[0] >> 4) & 0x7) * 10);

	// Address of minutes register
	buf[0] = DS3231_REG_MINUTES;

	// Request the contents of the minutes register (0x1)
	get_data(handle, DS3231_ADDR, buf, 1);

	// Convert BCD into int
	time->minutes = (buf[0] & 0x0F) + (((buf[0] >> 4) & 0x7) * 10);

	// Address of hours register
	buf[0] = DS3231_REG_HOURS;

	// Request the contents of the hours register (0x2)
	get_data(handle, DS3231_ADDR, buf, 1);
	time->hours = 0;

	// If bit 6 is not set, using 24 hour fmt
	if (!(buf[0] & (1 << 6))) {
		// If bit 5 is set, hours value is 20 + x
		if (buf[0] & (1 << 5)) {
			time->fmt = HOURS24;
			time->hours = 20;
		}
	} else {	// 12 hour format
		time->fmt = HOURS12;
		// If bit 5 is not set, time is AM
		if (!(buf[0] & (1 << 5))) {
			time->am_pm = AM;
		} else {	// Time is PM
			time->am_pm = PM;
		}
	}

	// Bit 4 indicates hours value is 10 + x
	// TODO: bits 4 and 5 in 24 hour mode should never be set together
	if (buf[0] & (1 << 4)) {
		time->hours = 10;
	}

	// Add the lower nibble to the time
	time->hours += buf[0] & 0x0F;
}

void ds3231_set_time(I2C_HandleTypeDef *handle, DS3231_Time *time) {
	uint8_t buf[2] = { 0 };

	// Update seconds register: send address (0x0) and data
	buf[0] = DS3231_REG_SECONDS;
	buf[1] = ((time->seconds / 10) << 4) + (time->seconds % 10);
	send_data(handle, DS3231_ADDR, buf, 2);

	// Update minutes register: send address (0x1) and data
	buf[0] = DS3231_REG_MINUTES;
	buf[1] = ((time->minutes / 10) << 4) + (time->minutes % 10);
	send_data(handle, DS3231_ADDR, buf, 2);

	// Update hours register: send address (0x2) and data
	buf[0] = DS3231_REG_HOURS;

	// Convert hours int to BCD.
	// Set 10-hour bit (if >= 10 hrs) in upper nibble, followed by remainder
	buf[1]  = (1 << 6)								// 12-hour format
			+ ((time->hours / 10) << 4) 			// 10 hour flag
			+ (time->hours % 10) 					// remainder hours
			+ ((PM == time->am_pm) ? (1 << 5) : 0);	// AM or PM

	send_data(handle, DS3231_ADDR, buf, 2);
}

void ds3231_get_date(I2C_HandleTypeDef *handle, DS3231_Date *date) {
	uint8_t buf[2] = { 0 };

	// Get the day of the week
	buf[0] = DS3231_REG_DAY;
	get_data(handle, DS3231_ADDR, buf, 1);
	date->dow = buf[0];

	// Get the date
	buf[0] = DS3231_REG_DATE;
	get_data(handle, DS3231_ADDR, buf, 1);

	date->dom = (((buf[0] & 0x30) >> 4) * 10) + (buf[0] & 0x0F);

	// Get the month
	buf[0] = DS3231_REG_MONTH;
	get_data(handle, DS3231_ADDR, buf, 1);

	date->month = (((buf[0] & 0x10) >> 4) * 10) + (buf[0] & 0x0F);

	static uint16_t century = 20;
	if (!!((buf[0] & 0x80) >> 7)) {
		century += 10;
		// Clear the century rollover flag
		buf[1] = (buf[0] ^ (1 << 7));
		buf[0] = DS3231_REG_MONTH;
		send_data(handle, DS3231_ADDR, buf, 2);
	}

	// Get the year
	buf[0] = DS3231_REG_YEAR;
	get_data(handle, DS3231_ADDR, buf, 1);

	date->year = (((buf[0] & 0xF0) >> 4) * 10) + (buf[0] & 0x0F);
	date->century = century;
}

void ds3231_set_date(I2C_HandleTypeDef *handle, DS3231_Date *date) {
	uint8_t buf[2] = { 0 };

	// Set the day of week
	buf[0] = DS3231_REG_DAY;
	buf[1] = (uint8_t)date->dow;
	send_data(handle, DS3231_ADDR, buf, 2);

	// Set the date
	buf[0] = DS3231_REG_DATE;
	buf[1] = ((date->dom / 10) << 4) + (date->dom % 10);
	send_data(handle, DS3231_ADDR, buf, 2);

	// Set the month
	buf[0] = DS3231_REG_MONTH;
	buf[1] = ((date->month / 10) << 4) + (date->month % 10);
	send_data(handle, DS3231_ADDR, buf, 2);

	// Set the year
	buf[0] = DS3231_REG_YEAR;
	buf[1] = ((date->year / 10) << 4) + (date->year % 10);
	send_data(handle, DS3231_ADDR, buf, 2);
}

void ds3231_get_temp(I2C_HandleTypeDef *handle, float *temp) {
	uint8_t buf = 0;

	buf = DS3231_REG_TEMPMSB;
	get_data(handle, DS3231_ADDR, &buf, 1);

	*temp = (float)((int8_t)buf);

	buf = DS3231_REG_TEMPLSB;
	get_data(handle, DS3231_ADDR, &buf, 1);

	*temp += (0.25f * (buf >> 6));
}
