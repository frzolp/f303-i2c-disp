/*
 * data_utils.c
 *
 *  Created on: Jan 30, 2019
 *      Author: Francis
 */

#include "data_utils.h"

extern void _Error_Handler(char *name, const char *func, int line);

void send_data(I2C_HandleTypeDef *handle, uint16_t address, uint8_t *data, uint16_t len) {
	while (HAL_OK != HAL_I2C_Master_Transmit(handle, address, data, len, 10000)) {
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			_Error_Handler(__FILE__, __func__, __LINE__);
		}
	}
}

void get_data(I2C_HandleTypeDef *handle, uint16_t address, uint8_t *data, uint16_t len) {
	// First, we send a one byte write of the register we want to read
	send_data(handle, address, data, 1);

	// Now we read the data
	while (HAL_OK != HAL_I2C_Master_Receive(handle, address, data, len, 10000)) {
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			_Error_Handler(__FILE__, __func__, __LINE__);
		}
	}
}
