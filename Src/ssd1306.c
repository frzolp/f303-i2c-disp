/*
 * ssd1306.c
 *
 *  Created on: Jan 21, 2019
 *      Author: Francis
 */

#include "ssd1306.h"
#include "courier_font.h"

extern void _Error_Handler(char *name, const char *func, int line);

/*
 * Display Initialization data.
 *
 * This array contains the values needed to get
 * the OLED display up and running from a reset
 */
static const uint8_t init[] = {
		0x00,						// Cmd byte
		SSD1306_DISPLAYOFF,			// Display off
		SSD1306_SETDISPLAYCLOCKDIV,	// Set Display Clock divider/Osc Frequency
		0x80,						// Osc Freq. value (default)
		SSD1306_SETMULTIPLEX,		// Set Multiplex Ratio
		SSD1306_LCDHEIGHT - 1,		// Set Display Start Line
		SSD1306_SETDISPLAYOFFSET,	// Display Offset
		0x0,						// Display Offset value (default)
		SSD1306_SETSTARTLINE | 0x0,	// Set Disp. Start Line (default)
		SSD1306_CHARGEPUMP,			// Charge Pump Setting
		0x14,
		SSD1306_MEMORYMODE,			// Memory Addressing Mode
		0x00,						// Mem. Addr. Mode val: horizontal addr. mode
		SSD1306_SEGREMAP | 0x1,		// Set Segment Remap (SEG0 -> col addr 127)
		SSD1306_COMSCANDEC,			// COM Out Scan Direction (63 -> 0)
		SSD1306_SETCOMPINS,			// COM Pin Hardware Config
		0x12,						// COM Pin Value: (default)
		SSD1306_SETCONTRAST,		// Set Contrast Control BANK0
		0xCF,						// Contrast value (207)
		SSD1306_SETPRECHARGE,		// Precharge Period
		0xF1,						// Precharge Period value
		SSD1306_SETVCOMDETECT,		// Set VCOMH Deselect Level
		0x00,						// VCOMH value: 0.65 * VCC
		SSD1306_DISPLAYALLON_RESUME,// Disable display all on
		SSD1306_NORMALDISPLAY,		// Set Normal Disp. (default, 1 in RAM = on)
		SSD1306_DEACTIVATE_SCROLL,	// Deactivate Scroll
		SSD1306_DISPLAYON			// Display On
};

/*
 * Display Data "preamble"
 *
 * The Adafruit library for the SSD1306 sends this data
 * before sending display data. Might not be needed *every* time.
 */
static const uint8_t dlist1[] = {
		0x00,
		SSD1306_PAGEADDR,
		0,
		0xFF,
		SSD1306_COLUMNADDR,
		0,
		SSD1306_LCDWIDTH - 1
};


extern uint8_t dispBuf[SSD1306_LCDWIDTH * ((SSD1306_LCDHEIGHT + 7) / 8) + 1];

void ssd1306_setup(I2C_HandleTypeDef *handle) {
	while (HAL_OK != HAL_I2C_Master_Transmit(handle, DISPLAY_ADDRESS, (uint8_t*)init, sizeof(init), 100000)) {
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			_Error_Handler(__FILE__, __func__, __LINE__);
		}
	}

	ssd1306_clear_display(handle);
}

void ssd1306_display(I2C_HandleTypeDef *handle) {
	while (HAL_OK != HAL_I2C_Master_Transmit(handle, DISPLAY_ADDRESS, (uint8_t*)dlist1, sizeof(dlist1), 100000)) {
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			_Error_Handler(__FILE__, __func__, __LINE__);
		}
	}

	// Set the data flag in the first byte to mark this as graphic data
	dispBuf[0] = 0x40;

	while (HAL_OK != HAL_I2C_Master_Transmit(handle, DISPLAY_ADDRESS, (uint8_t*)dispBuf, sizeof(dispBuf), 100000)) {
		if (HAL_I2C_ERROR_AF != HAL_I2C_GetError(handle)) {
			_Error_Handler(__FILE__, __func__, __LINE__);
		}
	}
}

void ssd1306_clear_display(I2C_HandleTypeDef *handle) {
	for (int x = 1; x < sizeof(dispBuf); x++) {
		dispBuf[x] = 0;
	}

	ssd1306_display(handle);
}

void ssd1306_invert_display(I2C_HandleTypeDef *handle, bool invert) {

}

void ssd1306_dim(I2C_HandleTypeDef *handle, bool dim) {

}

void ssd1306_draw_pixel(I2C_HandleTypeDef *handle, int16_t x, int16_t y) {

}

void ssd1306_draw_string(I2C_HandleTypeDef *handle, char *string, size_t len) {
	int disp_offset = 1;
	// Walk through each character in the string
	for (int i = 0; i < len; i++, string++) {
		// If we encounter a newline, move our memory offset to the first column of the next page
		if (*string == '\n') {
			disp_offset = (((disp_offset / SSD1306_LCDWIDTH) + 1) * SSD1306_LCDWIDTH) + 1;
			// We don't have anything else to do with this character, so move on to the next one
			continue;
		}
		// As we're using horizontal addressing, we split the character into pages of 8
		for (int page = 0; page < ((courierNew_8ptDescriptors[*string - ' '].height + 7) / 8); page++) {
			// Write a page of a character's data, column at a time
			for (int col = 0; col < (courierNew_8ptDescriptors[*string - ' '].width); col++) {
				int fontPosition = courierNew_8ptDescriptors[*string - ' '].offset + col + (page * courierNew_8ptDescriptors[*string - ' '].width);
				dispBuf[disp_offset + (page * SSD1306_LCDWIDTH) + col] = courierNew_8ptBitmaps[fontPosition];
			}
		}
		// Update the starting offset by the character's width
		disp_offset += courierNew_8ptDescriptors[*string - ' '].width;
	}

	ssd1306_display(handle);
}
